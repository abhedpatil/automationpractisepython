def helloWorldFuntion(name, lastname):  # parameters #parameters are placeholders we define in a function
    print('Hello! ', name, lastname)
    print(f'hi {name} {lastname}')
    print('Welcome to Functions')


helloWorldFuntion("John", "Marcus")  # arguments are the actual pieces of information we pass to the parameters
helloWorldFuntion("Gary", "Franz")
helloWorldFuntion("Jackie", "")


def square(number):
    return number * number


print(square(10))
