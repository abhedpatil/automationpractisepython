print('hello world')  # simple print statement

course = 'Python for beginners'  # by default declaration is a string
print(course)  # prints string value i.e. Python for beginners

another = course[:]  # string assignment
print(another)  # prints another i.e. Python for beginners

another = course[0:5]
print(another)  # prints another  i.e. Pytho

another = course[1:5]
print(another)  # prints another  i.e. ytho

yearOfBirth = 1982  # int declaration
yearofBirthString = '1990'  # string

print(yearOfBirth)  # 1982
print(yearofBirthString)  # 1990

yearOfBirth = int(yearofBirthString)  # convert string to int
print(yearOfBirth)  # 1990
yearofBirthString = str(yearOfBirth)  # convert int to string

print(type(yearOfBirth))  # <type 'int'>
print(type(yearofBirthString))  # <type 'str'>

is_published = True  # boolean

# birthYear = input('Birth year: ') #take input from user and save into variable birthYear
# print(birthYear)
print('Python course for "beginners"')  # Python course for "beginners"

multipleLines = '''
Hi,

This is python learning course

Pay attention
'''
print(multipleLines)  # prints string multiple lines ..everything

print(multipleLines.upper())  # upppercase
print(multipleLines.lower())  # lowercase
