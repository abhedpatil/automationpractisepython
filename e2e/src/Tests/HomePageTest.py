from e2e.src.Pages.HomePage import HomePage
from e2e.src.Tests.BaseTest import BaseTest


class HomePageTest(BaseTest):

    def test_validateUserNavigatedToHomePageAndAllElementsAreDisplayed(self):
        driver = self.driver

        homepage = HomePage(driver)
        homepage.validateUserNavigatedToHomePageAndAllElementsAreDisplayed()

    def test_valdiateUserCanLoginOnHomePage(self):
        driver = self.driver

        homepage = HomePage(driver)
        homepage.login('Admin', 'admin123')
