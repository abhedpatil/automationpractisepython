from datetime import datetime


class BasePage(object):

    def __init__(self):
        driver = self.driver

    def log(self, string):
        time = datetime.now()
        print("** ", time, string);
