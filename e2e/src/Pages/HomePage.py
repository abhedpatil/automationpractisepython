from e2e.src.Pages.BasePage import BasePage


class HomePage(BasePage):

    def __init__(self, driver):
        self.driver = driver

        self.userId = driver.find_element_by_css_selector("#txtUsername")
        self.password = driver.find_element_by_css_selector("#txtPassword")
        self.loginbtn = driver.find_element_by_css_selector("#btnLogin")
        self.divLogo = driver.find_element_by_css_selector("#divLogo")
        self.userLoginInfoText = driver.find_element_by_xpath('//*[@id="content"]/div[2]/span')

    def validateUserNavigatedToHomePageAndAllElementsAreDisplayed(self):
        assert self.userId.is_displayed(), "Validation failed: userid not displayed"
        print('Validated: userId displayed correctly')
        assert self.password.is_displayed(), "Validation failed: password not displayed"
        print('Validated: password displayed correctly')

    def login(self, userid, password):
        self.userId.clear()
        self.userId.send_keys(userid)
        self.password.clear()
        self.password.send_keys(password)
        self.loginbtn.click()
