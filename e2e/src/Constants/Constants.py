class Constants():
    loginInfo = '( Username : Admin | Password : admin123 )'
    forgotYourPassword = 'Forgot your password?'
    alternativeLogin = 'Alternative Login : '
    orangeHRM = '    OrangeHRM 4.3.2'
    year = '© 2005 - 2019 '
    website = 'http://www.orangehrm.com'
